import React from 'react';
import Main from './Container/Main/Main';
import {Route, Switch} from "react-router-dom";

function App() {
  return (
    <>
       <Switch>
        <Route path='/' exact component={Main}/>

      </Switch>
    </>
  );
}

export default App;
