import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addCity, deleteAllCities, deleteCity, getWheather } from '../../store/actions';
import './Main.css';
import {imgURL} from '../../config';

const Main=()=>{
    const dispatch=useDispatch();
    const {cities,wheatherList}=useSelector(state=>state.main);
    const [state,setState]=useState({
        city:''
    })
    useEffect(()=>{
        cities.map(item=>{
            dispatch(getWheather(item));
        })
    },[cities]);

    const deleteCityHandler=city=>{
        dispatch(deleteCity(city));
    };
    let citiesWheatherDiv=(
        <div className='main-list'>
            {
                wheatherList.map(item=>{
                    return(
                        <div key={item.id} className='main-box'>
                            <div className='main-box-inside'>
                                <h3>{item.name}</h3>
                                <button className='btn-del' onClick={()=>deleteCityHandler(item.name)}>Delete</button>
                            </div>
                            <img src={imgURL+'/img/wn/'+item.weather[0].icon+'@2x.png'} />
                            <p>{item.weather[0].description}</p>
                            <p>Temperature: {item.main.temp-273,15} C</p>
                            <p>Humidity: {item.main.humidity} %</p>
                            <p>Wind: {item.wind.speed} meter/sec</p>
                        </div>
                   );
                }) 
              
            }

        </div>
    )
    const inputHandler = event => {
        const {name, value} = event.target;
        setState(prevState => {
            return {...prevState, [name]: value}
        });
    };
    const formSubmitHandler = async event => {
        event.preventDefault();
        dispatch(addCity(state.city));
        setState({city:''})
    };
    const deleteAllCitiesHandler=()=>{
        dispatch(deleteAllCities());
    }
    return(
        <div className='container'>
            <form onSubmit={formSubmitHandler}>
                <input type='text' value={state.city} name='city' className='form-input' onChange={inputHandler}/>
                <button type='submit'className='btn'>Add City</button>
            </form>
            <button className='btn-del' onClick={deleteAllCitiesHandler}>Delete All Cities</button>
            {citiesWheatherDiv}
        </div>
    )
}
export default Main;