import axios from '../axios';
import {APPID} from '../config';

export const GET_WHEATHER_FOR_TODAY_SUCCESS='GET_WHEATHER_FOR_TODAY_SUCCESS';
export const GET_WHEATHER_FOR_TODAY_FAIL='GET_WHEATHER_FOR_TODAY_FAIL';

export const DELETE_ALL_CITIES='DELETE_ALL_CITIES';

export const ADD_CITY='ADD_CITY';

export const DELETE_CITY='DELETE_CITY';

export const getWheatherSuccess=value=>({type:GET_WHEATHER_FOR_TODAY_SUCCESS,value});
export const getWheatherFail=err=>({type:GET_WHEATHER_FOR_TODAY_FAIL,err});

export const getWheather=city=>{
    return async dispatch=>{
        try{
            const res=await axios.get('/data/2.5/weather?q='+city+'&APPID='+APPID);
            dispatch(getWheatherSuccess(res.data));
        }
        catch(e){
            dispatch(getWheatherFail(e));
        }
    }
}

export const deleteCity=value=>({type:DELETE_CITY,value});

export const addCity=value=>({type:ADD_CITY,value});

export const deleteAllCities=()=>({type:DELETE_ALL_CITIES});

