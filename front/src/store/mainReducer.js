import { ADD_CITY, DELETE_ALL_CITIES, DELETE_CITY, GET_WHEATHER_FOR_TODAY_FAIL, GET_WHEATHER_FOR_TODAY_SUCCESS } from "./actions";

const initialState={
    cities:[],
    wheatherList:[],
    err:null
}

const mainReducer=(state=initialState,action)=>{
    switch(action.type){
        case GET_WHEATHER_FOR_TODAY_SUCCESS:
            return {...state,
                err:null,
                wheatherList:[...state.wheatherList,action.value]
             };
        case GET_WHEATHER_FOR_TODAY_FAIL:
            return {...state, err:action.err};
        case DELETE_CITY:{
            return {...state,
                wheatherList:[],
                err:null,
                cities:state.cities.filter(item=>item !==action.value),
            }
        }
        case ADD_CITY:
            return {...state,
                wheatherList:[],
                err:null,
                cities:[...state.cities,action.value]
            }
        case DELETE_ALL_CITIES:
            return {...state, cities:[],err:null,wheatherList:[]}

        default:
            return state;
    }
}
export default mainReducer;